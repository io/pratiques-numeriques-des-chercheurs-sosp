# Analyse de questionnaire : enquête sur les pratiques numériques des chercheurs

V1 complète 2022-07-22

## Présentation du Notebook

Démarche d'analyse de données d'une enquête par questionnaire.

L'objectif est de présenter la démarche de chargement, de transformation et d'analyse des données avec pour objectif la production d'une typologie des répondants sur leurs pratiques numériques.

*Les données utilisées sont celles collectées en 2020 par le collectif du projet* **State of Open Science Practices in France**  *Mariannig Le Béchec - Aline Bouchard - Philippe Charrier - Claire Denecker - Gabriel Gallezot - Stéphanie Rennes [mise à disposition sur la plateforme Zenodo](https://zenodo.org/record/5827206) qui ont donné lieu à un* [rapport](https://hal.archives-ouvertes.fr/hal-03545512v1).

Niveau du Notebook : débutant/intermédiaire

Principales notions vues :

- manipulation de tableaux
- statistiques descriptives
- petites visualisations
- analyse factorielle (ACM)
- classification (hiérarchie ascendante)


## Contenu du notebook

Présentation générale

Éléments du contexte (données et bibliothèques)

1. **Transformation des données**

1.1. Télécharger les données

1.2. Transformer les données "brutes" en un tableau utilisable

1.3. Exploration initiale

1.4. Identification des variables et recodages

1.5. Recodage des variables

2. **Analyse descriptive des données**

2.1. Univariée : tri à plat

2.2. Bivariée : croisement de variables

2.3. Visualisations

3. **Classification en profils de répondants**

3.1. Analyse factorielle sur les données (ACM)

3.2. Classification hiérarchique ascendante sur ACM (HCPC)

3.3. Analyse des profils

3.4. Identification des clusters et visualisations finalisées

3.5. Autres stratégies de classification (K-Means)

4. **Analyse des données non-structurées (champ ouvert)**

4.1. Extraire la distribution des mots

4.2. Identifier les usagers de Python par rapport aux profils

Conclusion

## Crédits

### Auteurs

Ce notebook a été réalisé par Émilien Schultz (@EmilienSchultz) et Mathieu Morey, avec la contribution d'Antoine Blanchard.

### Licence

MIT License

Copyright (c) 2022 Huma-Num Lab

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
